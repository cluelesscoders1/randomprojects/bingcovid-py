# BingCovid

A python Discord bot to report some findings of the COVID-19 virus, using the BING API.

## Pre-reqs

It is best to use virtual env. Once started use `python3 -m pip install -r requirements.txt`. 

## Use

1. Create a Discord bot through the [Discord Developer portal](https://discordapp.com/developers/applications/).
1. Copy unique Discord key to a file named `.env` with `export DISCORD_TOKEN=<key>` as it's contents. 
1. `source .env`
1. Start bot

 
 bingbot accepts 6 arguments:

`bingint` -Returns "Total Global" statistics

`bingint countryname` -Returns "Total Country" statistics

`bingint TopX` -Returns Top X countries based on total cases

`bingus` -Returns "Total United States" statistics

`bingus statename` -Returns "Total US State" statistics

`bingus statename countyname` -Returns "Total County" stats within given state

`bingus TopX` -Returns Top X states based on total cases

### Notes

Possible reportable statistics for each query, need to modify output function to include more.

    id
    displayName
    areas
    totalConfirmed
    totalDeaths
    totalRecovered
    totalRecoveredDelta
    totalDeathsDelta
    totalConfirmedDelta

## Changes

| Date           | Change                                                         |
|----------------|----------------------------------------------------------------|
| April 8, 2020  | Converted into Discord bot.                                    |
| April 8, 2020  | Added US counties within a state.                              |
| April 11, 2020 | Added numerical commas and iconography.                        |
| April 12, 2020 | Added country flags.                                           |
| April 14, 2020 | Added ability to find Top X in Countries or States.            |
| April 15, 2020 | Reformatted topx return and help menu to use formatted tables. |
