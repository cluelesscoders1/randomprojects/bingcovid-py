from prettytable import PrettyTable
import locale
import requests
import discord
import json
import flag
import re
import os

locale.setlocale(locale.LC_ALL, "")
TOKEN = os.getenv('DISCORD_TOKEN')

#bingCovid = requests.get("https://bing.com/covid/data")

with open("states.json",'r') as statefile:
	statejson = json.load(statefile)

def bingOutput(region):
	outFormat = ""
	addFlag = ""
	#separate all values and change any none/null values to 0
	outList = []
	outList.append(region['displayName'])
	outList.append(region['totalConfirmed'])
	outList.append(region['totalConfirmedDelta'])
	outList.append(region['totalDeaths'])
	outList.append(region['totalDeathsDelta'])
	outList.append(region['totalRecovered'])
	outList = [0 if x == None else x for x in outList]
	
	#get flag emoji if global country
	try:
		if region['parentId'] == "world":
			uniFlags = requests.get("https://flagpedia.net/download/country-codes-case-upper.json")
			for key, value in uniFlags.json().items():
				if value == outList[0]:
					countryCode = key
			addFlag = flag.flag(countryCode)
	except:
		pass

	#return all values and add commas to number values
	outFormat = f"{addFlag} {outList[0]} Covid-19 Statistics\n"
	outFormat += "🤢 Total Cases: " + str(f"{outList[1]:n}")
	outFormat += " -- New Cases: " + str(f"{outList[2]:n}")
	outFormat += "  💀 Total Deaths: " + str(f"{outList[3]:n}")
	outFormat += " -- New Deaths: " + str(f"{outList[4]:n}")
	outFormat += "  😅 Total Recovered: " + str(f"{outList[5]:n}")
	return outFormat

def getTopX(region, topx):
	bingCovid = requests.get("https://bing.com/covid/data")
	sortedList = [{'id':'belowzero', 'totalConfirmed':-1}]
	
	# table output formatting
	topTable = PrettyTable()
	topTable.field_names = [
		"State",
		"Total Cases",
		"New Cases",
		"Total Deaths",
		"New Deaths",
		"Total Recovered"]
	topTable.align["State"] = "l"
	topTable.align["Total Cases"] = "r"
	topTable.align["New Cases"] = "r"
	topTable.align["Total Deaths"] = "r"
	topTable.align["New Deaths"] = "r"
	topTable.align["Total Recovered"] = "r"


	if region == "states":
		for state in bingCovid.json()['areas'][0]['areas']:
			for i in sortedList:
				if state['totalConfirmed'] > i['totalConfirmed']:
					sortedList.insert(sortedList.index(i),state)
					break

	elif region == "countries":
		for country in bingCovid.json()['areas']:
			for i in sortedList:
				if country['totalConfirmed'] > i['totalConfirmed']:
					sortedList.insert(sortedList.index(i),country)
					break
	if topx == "":
		topx = "5"

	for i in range(int(topx)):
		topTable.add_row([
			sortedList[i]['displayName'],
			f"{sortedList[i]['totalConfirmed']:n}",
			f"{sortedList[i]['totalConfirmedDelta']:n}",
			f"{sortedList[i]['totalDeaths']:n}",
			f"{sortedList[i]['totalDeathsDelta']:n}",
			f"{sortedList[i]['totalRecovered']:n}"])

	topXoutmsg = f'```\n{topTable.get_string(title=f"Covid-19 Top 5 {region} by total cases")}```'

	return topXoutmsg

def usStateStats(state):
	bingCovid = requests.get("https://bing.com/covid/data")
	stateOut = ""
	getState = state.upper()
	try:
		getState = statejson['states'][(getState[:2])]
		for i in bingCovid.json()['areas'][0]['areas']:
			if i['id'] == getState:
				stateOut = bingOutput(i)
	except:
		stateOut = getState + " is not in the state list.  Try again."
	return stateOut

def usCountyStats(state, county):
	bingCovid = requests.get("https://bing.com/covid/data")
	countyOut = ""
	getState = state.upper()
	try:
		getState = statejson['states'][(getState[:2])]
		getCounty = county.lower() + "_" + getState
		for i in bingCovid.json()['areas'][0]['areas']:
			if i['id'] == getState:
				for sc in i['areas']:
					if sc['id'] == getCounty:
						countyOut = bingOutput(sc)
	except:
		countyOut = getCounty + " is not in the county list.  Try again."
	return countyOut

def countryStats(country):
	bingCovid = requests.get("https://bing.com/covid/data")
	countryOut = ""
	if country == "":
		i = bingCovid.json()
		countryOut = bingOutput(i)
	else:
		try:
			for i in bingCovid.json()['areas']:
				if i['id'] == country:
					countryOut = bingOutput(i)
		except:
			countryOut = country + " is not in the country list. Try again."
	return countryOut

def main():
	helpTable = PrettyTable()
	helpTable.field_names = ["Command", "Description"]
	helpTable.align["Command"] = "l"
	helpTable.align["Description"] = "l"
	helpTable.add_row(["bingus", "Returns USA statistics updated from Bing tracker"])
	helpTable.add_row(["bingus <XX>", "Returns US State statistics using 2-letter abbreviation"])
	helpTable.add_row(["bingus TopX", "Returns top X US States ranked by total cases (no X defaults to 5)"])
	helpTable.add_row(["bingint", "Returns Global total statistics updated from Bing tracker"])
	helpTable.add_row(["bingint <countryname>", "Returns specific Country statistics using full country name"])
	helpTable.add_row(["bingint TopX", "Returns top X countries ranked by total cases (no X defaults to 5)"])
	bingCommands = f'```\n{helpTable.get_string(title="Bing Covid-19 Lookup Commands")}```'

	client = discord.Client()
	bingUSPattern = re.compile("bingus")
	bingIntPattern = re.compile("bingint")

	@client.event
	async def on_ready():
		print(f"{client.user} has connected to Discord!")
	
	@client.event
	async def on_message(bingLookup):
		if bingLookup.content.lower() == "binghelp":
			await bingLookup.channel.send(bingCommands)
		
		elif bingUSPattern.match(bingLookup.content.lower()):
			getState = bingLookup.content[7:]
			if getState == '':
				print("Country: unitedstates")
				await bingLookup.channel.send(countryStats("unitedstates"))
			elif getState.lower().startswith("top"):
				print(f"Top {getState[3:]} states.")
				await bingLookup.channel.send(getTopX("states",getState[3:]))
			elif " ".join(getState.split()).count(" ") > 0:
				getCounty = " ".join(getState.split()).split()[1]
				print(f"State: {getState} - County: {getCounty}")
				await bingLookup.channel.send(usCountyStats(getState, getCounty))
			else:
				print(" ".join(getState.split()).count(" "))
				print(f"State: {getState}")
				await bingLookup.channel.send(usStateStats(getState))
		
		elif bingIntPattern.match(bingLookup.content.lower()):
			getCountry = bingLookup.content[8:].lower().replace(" ", "")
			if getCountry.lower().startswith("top"):
				print(f"Top {getCountry[3:]} countries.")
				await bingLookup.channel.send(getTopX("countries", getCountry[3:]))
			else:
				print(f"Country: {getCountry}")
				await bingLookup.channel.send(countryStats(getCountry))
	
	client.run(TOKEN)


if __name__ == '__main__':
	main()
